package business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import observer.Observable;
import data.FileWriter;
import model.MenuItem;
import model.Order;

/**
 * @author Bouaru Radu
 * @invariant isWellFormed()
 */
public final class Restaurant extends Observable implements IRestaurantProcessingAdministrator, IRestaurantProcessingWaiter, Serializable {

	private static final long serialVersionUID = 6529685098267757690L;
	/**
	 * HashMap-ul care va contine colectia de comenzi impreuna cu meniurile comandate
	 */
	private Map<Order, Collection<MenuItem>> restaurant = new HashMap<Order, Collection<MenuItem>>();
	/**
	 * Lista ce va contine toate meniurile existente in restaurant
	 */
	private ArrayList<MenuItem> menu = new ArrayList<MenuItem>();
	/**
	 * Class invariant
	 * 
	 * @pre true
	 * @post @nochange
	 */
	private boolean isWellFormed() {
		if (restaurant == null)
			return false;
		return true;
	}
	/**
	 * Metoda care returneaza ID-ul curent al unei comenzi
	 * @return ID-ul pentru o comanda noua
	 */
	public int getOrderID() {
		return this.restaurant.size();
	}

	@Override
	public void createOrder(Order order, ArrayList<MenuItem> orderedMenu) {
		assert order != null && restaurant != null && isWellFormed();	
		
		int lastRestaurantSize = restaurant.size();
		restaurant.put(order, orderedMenu);
		notifyObservers(order, orderedMenu);
		assert restaurant.size() == (lastRestaurantSize + 1);
		assert restaurant.get(order) != null;
		assert isWellFormed();
	}

	@Override
	public double computeOrderPrice(Order order) {
		assert order != null && restaurant != null && isWellFormed();
		double orderPrice = 0;
		ArrayList<MenuItem> menuItems = (ArrayList<MenuItem>) restaurant.get(order);
		for(MenuItem menuItem: menuItems)
			orderPrice += menuItem.computePrice();
		assert orderPrice != 0;
		assert isWellFormed();
		return orderPrice;
	}

	@Override
	public void generateBill(Order order) {
		assert order != null && restaurant != null && isWellFormed();
		ArrayList<MenuItem> menuItems = (ArrayList<MenuItem>) restaurant.get(order);
		FileWriter.generateBill(menuItems, order.getOrderID(), computeOrderPrice(order));
		assert isWellFormed();
	}
	/**
	 * Metoda care va returna o lista cu produsele comandate in cadrul unei comenzi
	 * @param order Comanda pentru care se cauta lista de produse
	 * @return Produsele comandate cautate
	 */

	public ArrayList<MenuItem> getOrderItems(Order order) {
		assert restaurant != null && isWellFormed();
		ArrayList<MenuItem> menuItems = (ArrayList<MenuItem>) restaurant.get(order);
		assert isWellFormed();
		
		return menuItems;
	}

	@Override
	public void createMenuItem(MenuItem menuItem) {
		assert menuItem != null && restaurant != null && isWellFormed();
		int menuSize = 0;
		if(this.menu!= null)
			menuSize  = this.menu.size();
		
		ArrayList<MenuItem> menuItems = getMenus();
		if(menuItems != null)		
			menuItems.add(menuItem);
		else
		{
			menuItems = new ArrayList<MenuItem>();
			menuItems.add(menuItem);
		}
		assert this.menu.size() == (menuSize + 1);
		assert this.menu.contains(menuItem) == true;
		assert isWellFormed();
	}

	@Override
	public void deleteMenuItem(MenuItem menu) {
		assert menu != null && restaurant != null && isWellFormed();
		int menuSize = 0;
		if(this.menu!= null)
			menuSize  = this.menu.size();
		
		ArrayList<MenuItem> menuItems = getMenus();
		
		MenuItem itemToRemove = null;
		if(menuItems != null && menuItems.size() > 0)		
				for(MenuItem i: menuItems)
					if(i.getMenuItemName().equalsIgnoreCase(menu.getMenuItemName()))
					{
						itemToRemove = i;
						break;
					}
		if(itemToRemove != null)
			menuItems.remove(itemToRemove);

		assert this.menu.size() == (menuSize - 1);
		assert this.menu.contains(menu) != true;
		assert isWellFormed();
	}

	@Override
	public void editMenuItem(MenuItem currentMenu, MenuItem newMenu) {
		assert currentMenu != null && newMenu != null && restaurant != null && isWellFormed();
		
		int menuSize = this.menu.size();
		
		deleteMenuItem(currentMenu);
		createMenuItem(newMenu);
		
		assert this.menu.size() == menuSize;
		assert this.menu.contains(currentMenu) == false;
		assert this.menu.contains(newMenu) == true;
		assert isWellFormed();
	}
	/**
	 * Metoda care va returna o lista cu toate comenzile din sistem
	 * @return Lista cu comenzi
	 */
	public ArrayList<Order> getOrders(){
		assert restaurant != null && isWellFormed();
		
		ArrayList<Order> orders = new ArrayList<Order>();
		for(Order order: restaurant.keySet())
			orders.add(order);
		
		assert isWellFormed();
		return orders;
	}
	/**
	 * Metoda care va returna o lista cu toate meniurile din sistem
	 * @return Lista cu meniuri
	 */
	public ArrayList<MenuItem> getMenus(){
		assert restaurant != null && isWellFormed();
		assert isWellFormed();
		return this.menu;
	}	
	/**
	 * Metoda care va seta lista cu meniuri de la o lista predefinita
	 * @param menu Lista cu meniuri care se va incarca 
	 */
	public void setMenus(ArrayList<MenuItem> menu)
	{
		assert restaurant != null && isWellFormed();		
		this.menu = menu;
		assert isWellFormed();
	}
}
