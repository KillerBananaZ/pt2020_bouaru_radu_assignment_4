package business;

import java.util.ArrayList;

import model.MenuItem;
import model.Order;

public interface IRestaurantProcessingWaiter {
	/**
	 * Creaza o comanda noua, bazata pe lista de produse comandate din meniu
	 * @pre order != null && restaurant != null && isWellFormed()
	 * @post restaurant.size() == restaurant.size() @pre  + 1
	 * @post restaurant.get(order) != null
	 * @post isWellFormed()
	 */
	public void createOrder(Order order, ArrayList<MenuItem> menu);
	/**
	 * Calculeaza pretul total al unei comenzi
	 * @pre order != null && restaurant != null && isWellFormed()
	 * @post orderPrice != 0
	 * @post isWellFormed()
	 */
	public double computeOrderPrice(Order order);
	/**
	 * Genereaza factura pentru o comanda
	 * @pre order != null && restaurant != null && isWellFormed()
	 * @post isWellFormed()
	 */
	public void generateBill(Order order);
}
