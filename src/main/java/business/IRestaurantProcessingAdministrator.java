package business;

import model.MenuItem;

public interface IRestaurantProcessingAdministrator {
	/**
	 * 
	 * Creeaza un produs de baza sau compus nou care va fi introdus in meniu
	 * @pre menuItem != null && restaurant != null && isWellFormed()
	 * @post menu.size() == menu.size()@pre + 1
	 * @post menu.get(menuItem) != null
	 * @post isWellFormed()
	 */
	public void createMenuItem(MenuItem menuItem);
	/**
	 * Sterge un produs de baza sau compus deja existent in meniu
	 * @pre menuItem != null && restaurant != null && isWellFormed();
	 * @post menu.size() == menu.size()@pre - 1
	 * @post menu.contains(menuItem) != true
	 * @post isWellFormed()
	 */
	public void deleteMenuItem(MenuItem menuItem);
	/**
	 * Editeaza un produs de baza sau compus deja in meniu 
	 * @pre currentMenu != null && newMenu != null && restaurant != null && isWellFormed();
	 * @post menu.size() == menu.size()@pre
	 * @post menu.contains(currentMenu) == false
	 * @post menu.contains(newMenu) == true
	 */
	public void editMenuItem(MenuItem currentMenu, MenuItem newMenu) ;

}
