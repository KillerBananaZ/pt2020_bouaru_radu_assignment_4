package start;

import java.text.ParseException;
import business.Restaurant;
import controller.MainController;
import data.RestaurantSerializator;
import view.MainViewFrame;

public class Start {

	public static void main(String[] args) throws ParseException {
		if(args.length == 0)
		{
			System.out.println("No input arguments!");
		}
		else
		{
			Restaurant restaurant = new Restaurant();
			restaurant = RestaurantSerializator.deserialize(args[0]);
			new MainController(new MainViewFrame("Restaurant Management"), restaurant, args[0]);
		}
	}
}
