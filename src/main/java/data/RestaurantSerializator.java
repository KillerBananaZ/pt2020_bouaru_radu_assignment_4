package data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import business.Restaurant;

public class RestaurantSerializator {

	public static void serialize(Restaurant restaurant, String fileName) {
		try {
			FileOutputStream file = new FileOutputStream(fileName);
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(restaurant);
			out.close();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static Restaurant deserialize(String fileName) {
		Restaurant deserializedRestaurant = new Restaurant();		
		try {
			FileInputStream file = new FileInputStream(fileName);
			ObjectInputStream in = new ObjectInputStream(file);
			deserializedRestaurant = (Restaurant)in.readObject();
			in.close();
			file.close();
			return deserializedRestaurant;
		} catch (IOException | ClassNotFoundException e) {
			System.out.println("Nothing to deserialize");
		}
		return new Restaurant();
	}
}
