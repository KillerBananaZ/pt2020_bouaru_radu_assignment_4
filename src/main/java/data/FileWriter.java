package data;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import model.MenuItem;

public class FileWriter {

	public static void generateBill(ArrayList<MenuItem> menuItems, int orderID, double orderPrice) {
		try {
			PrintWriter outputStream = new PrintWriter("Order " + orderID + " bill.txt");
			for(MenuItem menuItem: menuItems)
				outputStream.println(menuItem.getMenuItemName() + ": " + menuItem.computePrice() + " lei");
			outputStream.println("\n------------------------------------------");
			outputStream.println("Total price for order " + orderID + ": " + orderPrice +" lei");
			
			outputStream.close();
		}
		catch(IOException ex)
		{
			System.out.println(ex.getMessage());
		}
	}
}
