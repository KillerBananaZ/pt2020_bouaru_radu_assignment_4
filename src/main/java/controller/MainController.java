package controller;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import business.Restaurant;
import data.RestaurantSerializator;
import model.BaseProduct;
import model.CompositeProduct;
import model.MenuItem;
import model.Order;
import view.ChefPane;
import view.MainViewFrame;

public class MainController {

	protected MainViewFrame view;
	protected Restaurant restaurant;
	protected String fileName;

	public MainController(MainViewFrame view, Restaurant restaurant, String fileName) {
		this.view = view;
		this.restaurant = restaurant;
		this.fileName = fileName;

		this.view.setTable(createProductTable(restaurant.getMenus()));
		this.view.setAllBaseProducts(createBaseProductsList(restaurant.getMenus()));
		this.view.setBaseProducts(createBaseProductsList(restaurant.getMenus()));
		this.view.setMenuTable(createProductTable(restaurant.getMenus()));
		this.view.setOrderTable(createOrdersTable());
		this.view.setChefPane(createChefPane());
		this.view.setAddBaseProductActionListener(new AddButtonActionListener());
		this.view.setAddCompositeProductActionListener(new AddCompositeActionListener());
		this.view.setDeleteButtonActionListener(new DeleteButtonActionListener());
		this.view.setEditButtonActionListener(new EditButtonActionListener());
		this.view.setAddOrderButtonActionListener(new CreateOrderActionListener());
		this.view.setGenerateButtonActionListener(new GenerateBillActionListener());
		this.view.setCloseAction(new WindowClosingActionListener());
		this.view.setVisible(true);

	}

	public ChefPane createChefPane() {
		ChefPane cp;
		if (view.getChefPane() != null)
			cp = view.getChefPane();
		else
		{
			cp = new ChefPane(restaurant);
			restaurant.addObserver(cp);
		}
		return cp;
	}

	public JTable createProductTable(ArrayList<MenuItem> objects) {
		DefaultTableModel productsTableModel = new DefaultTableModel();

		String[] tableHeaders = { "Product name", "Product price" };
		productsTableModel.setColumnIdentifiers(tableHeaders);

		Object rowData[] = new Object[2];

		for (MenuItem menuItem : objects) {
			rowData[0] = menuItem.getMenuItemName();
			rowData[1] = menuItem.computePrice();
			productsTableModel.addRow(rowData);
		}
		final JTable table = new JTable(productsTableModel);
		table.setDefaultEditor(Object.class, null);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				view.setDeleteProductField(table.getValueAt(table.getSelectedRow(), 0).toString());
				view.setEditedProductName(table.getValueAt(table.getSelectedRow(), 0).toString());
			}
		});
		table.getTableHeader().setFont(new Font("Verdana", Font.PLAIN, 15));
		return table;
	}

	private int order;

	public JTable createOrdersTable() {
		DefaultTableModel ordersTableModel = new DefaultTableModel();
		String[] tableHeaders = { "Order ID", "Order date", "Order table" };
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		ordersTableModel.setColumnIdentifiers(tableHeaders);

		Object rowData[] = new Object[3];
		for (Order o : restaurant.getOrders()) {
			rowData[0] = o.getOrderID();
			rowData[1] = o.getDate().format(formatter);
			rowData[2] = o.getTable();
			ordersTableModel.addRow(rowData);
		}
		final JTable table = new JTable(ordersTableModel);
		TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
		table.setRowSorter(sorter);
		List<RowSorter.SortKey> sortKeys= new ArrayList<>();
		sortKeys.add(new RowSorter.SortKey(1,  SortOrder.ASCENDING));
		sorter.setSortKeys(sortKeys);
		sorter.sort();
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				order = Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString());
			}
		});
		table.setDefaultEditor(Object.class, null);
		table.getTableHeader().setFont(new Font("Verdana", Font.PLAIN, 15));
		return table;
	}

	public static JList<MenuItem> createBaseProductsList(ArrayList<MenuItem> objects) {
		JList<MenuItem> newList = new JList<>();
		DefaultListModel<MenuItem> model = new DefaultListModel<>();
		for (MenuItem i : objects)
			if (i instanceof BaseProduct)
				model.addElement(i);
		newList.setModel(model);
		return newList;
	}

	public void updateUI() {
		this.view.setTable(createProductTable(restaurant.getMenus()));
		this.view.setMenuTable(createProductTable(restaurant.getMenus()));
		this.view.setAllBaseProducts(createBaseProductsList(restaurant.getMenus()));
		this.view.setBaseProducts(createBaseProductsList(restaurant.getMenus()));
	}

	private class AddButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(view.getBaseProductName().isEmpty() || view.getBaseProductPrice() == 0)
			{
				JOptionPane.showMessageDialog(null, "Invalid inputs, check them again!");
				return;
			}
			String productName = view.getBaseProductName();
			Double productPrice = view.getBaseProductPrice();
			MenuItem newMenuItem = new BaseProduct(productName, productPrice);
			restaurant.createMenuItem(newMenuItem);
			updateUI();
			view.clearAllInputs();
		}
	}

	private class AddCompositeActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(view.getCompositeProductName().isEmpty() || view.getList() == null)
			{
				JOptionPane.showMessageDialog(null, "Invalid inputs, check them again!");
				return;
			}
			String newCompositeName = view.getCompositeProductName();
			ArrayList<MenuItem> selectedElements = view.getList();

			MenuItem newComposite = new CompositeProduct(newCompositeName);
			for (MenuItem item : selectedElements)
				newComposite.add(item);

			restaurant.createMenuItem(newComposite);
			updateUI();
			view.clearAllInputs();
		}
	}

	private class DeleteButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String productName = view.getDeleteProductField();
			MenuItem toBeDeleted = null;
			for (MenuItem mi : restaurant.getMenus())
				if (mi.getMenuItemName().equalsIgnoreCase(productName)) {
					toBeDeleted = mi;
					break;
				}

			ArrayList<MenuItem> toBeRemovedAll = new ArrayList<MenuItem>();
			if (toBeDeleted instanceof BaseProduct)
				for (MenuItem mi : restaurant.getMenus())
					if (mi instanceof CompositeProduct)
						if (((CompositeProduct) mi).getMenuItemList().contains(toBeDeleted))
							toBeRemovedAll.add(mi);

			for (MenuItem mi : toBeRemovedAll)
				restaurant.deleteMenuItem(mi);

			restaurant.deleteMenuItem(toBeDeleted);
			updateUI();
			view.clearAllInputs();
		}
	}

	private class EditButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			ArrayList<MenuItem> selectedElements = view.getList();
			MenuItem itemToBeEdited = null;
			for (MenuItem mi : restaurant.getMenus())
				if (mi.getMenuItemName().equalsIgnoreCase(view.getEditedProductName())) {
					itemToBeEdited = mi;
					break;
				}
			restaurant.deleteMenuItem(itemToBeEdited);
			if (itemToBeEdited instanceof BaseProduct) {
				System.out.println("Base");
				restaurant.createMenuItem(
						new BaseProduct(view.getNewProductName(), Double.parseDouble(view.getNewProductPrice())));
			} else {
				System.out.println("Composite");
				MenuItem newComposite = new CompositeProduct(view.getNewProductName());
				for (MenuItem mi : selectedElements)
					newComposite.add(mi);
				restaurant.createMenuItem(newComposite);
			}
			updateUI();
			view.clearAllInputs();
		}
	}
	private class CreateOrderActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(view.getTableName().isEmpty() || view.getOrderedList().isEmpty())
			{
				JOptionPane.showMessageDialog(null, "Invalid inputs, check them again!");
				return;
			}
			ArrayList<String> selectedElements = view.getOrderedList();

			view.setChefPane(createChefPane());

			ArrayList<MenuItem> items = new ArrayList<MenuItem>();
			for (MenuItem mi : restaurant.getMenus())
				for (String s : selectedElements)
					if (mi.getMenuItemName().equalsIgnoreCase(s))
						items.add(mi);
			String table = view.getTableName();
			int orderID = restaurant.getOrderID();
			Order newOrder = new Order(orderID, LocalDateTime.now(), table);
			restaurant.createOrder(newOrder, items);

			view.setOrderTable(createOrdersTable());
		}
	}

	private class GenerateBillActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Order toGenerateBill = null;
			for (Order o : restaurant.getOrders())
				if (o.getOrderID() == order) {
					toGenerateBill = o;
					break;
				}
			restaurant.generateBill(toGenerateBill);
		}

	}

	private class WindowClosingActionListener implements WindowListener {

		@Override
		public void windowOpened(WindowEvent e) {
			System.out.println("Opened");
			restaurant = RestaurantSerializator.deserialize(fileName);
		}

		@Override
		public void windowClosing(WindowEvent e) {
			RestaurantSerializator.serialize(restaurant, fileName);
			System.out.println("Serialized");
		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowActivated(WindowEvent e) {
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
		}
	}

}
