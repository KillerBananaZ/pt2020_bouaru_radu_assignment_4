package model;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable{

	 private static final long serialVersionUID = 6529685098267757690L;
	
	private String productName;
	private double productPrice;
	
	public BaseProduct(String productName, double productPrice) {
		super(productName);
		this.productName = productName;
		this.productPrice = productPrice;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return productName;
	}

	@Override
	public double computePrice() {
		return this.productPrice;		
	}
	@Override
	public void displayMenu() {
		System.out.println(getMenuItemName()+" : " + computePrice());
	}

}
