package model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Order implements Serializable {

	private static final long serialVersionUID = 6529685098267757690L;

	private int orderID;
	private LocalDateTime date;
	private String table;

	public Order(int orderID, LocalDateTime date, String table) {
		super();
		this.orderID = orderID;
		this.date = date;
		this.table = table;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	@Override
	public int hashCode() {
		final int prime = 67;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + orderID;
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		return result;
	}

	public String parseDate(LocalDateTime date) {
		StringBuilder sb = new StringBuilder();
		sb.append(date.getDayOfMonth() + "-" + date.getMonth() + "-" + date.getYear());
		sb.append(" la ora ");
		String newMinutes = "";
		String newSeconds = "";
		if (date.getMinute() < 10)
			newMinutes = "0" + date.getMinute();
		else
			newMinutes = date.getMinute() + "";
		if (date.getSecond() < 10)
			newSeconds = "0" + date.getSecond();
		else
			newSeconds = date.getSecond() + "";
		sb.append(date.getHour() + ":" + newMinutes + ":" + newSeconds);
		return sb.toString();
	}

	@Override
	public String toString() {
		return "Order " + this.orderID + " la masa " + this.table + " facuta in data de " + parseDate(this.date);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (orderID != other.orderID)
			return false;
		if (table == null) {
			if (other.table != null)
				return false;
		} else if (!table.equals(other.table))
			return false;
		return true;
	}

}
