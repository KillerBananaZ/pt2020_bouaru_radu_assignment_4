package model;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {

	private static final long serialVersionUID = 6529685098267757690L;

	private String menuItemName;

	public abstract double computePrice();

	public abstract void displayMenu();

	public MenuItem(String name) {
		this.menuItemName = name;
	}

	public String getMenuItemName() {
		return menuItemName;
	}

	public void add(MenuItem menuItem) {
		throw new UnsupportedOperationException();
	}
}
