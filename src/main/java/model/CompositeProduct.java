package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem implements Serializable {

	private static final long serialVersionUID = 6529685098267757690L;

	private List<MenuItem> menuItemList = new ArrayList<MenuItem>();

	public CompositeProduct(String name) {
		super(name);
	}

	@Override
	public double computePrice() {
		double price = 0;
		for (MenuItem menuItem : this.menuItemList)
			price += menuItem.computePrice();
		return price;
	}

	@Override
	public void add(MenuItem menuItem) {
		this.menuItemList.add(menuItem);
	}

	@Override
	public void displayMenu() {
		System.out.println(getMenuItemName() + ":");
		for (MenuItem menuItem : this.menuItemList)
			menuItem.displayMenu();

		System.out.println("Pret total: " + computePrice() + " lei");
		System.out.println();
	}
	public List<MenuItem> getMenuItemList(){
		return this.menuItemList;
	}
}
