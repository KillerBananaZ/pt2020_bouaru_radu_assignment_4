package view;

import observer.Observer;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import business.Restaurant;
import model.MenuItem;
import model.Order;

public class ChefPane extends JPanel implements Observer{

	private static final long serialVersionUID = 1L;
	
	private JTextArea orderArea = new JTextArea();
	public ChefPane(Restaurant restaurant) {	
		setLayout(null);		
		orderArea.setBounds(5, 5, 900, 600);
		orderArea.setFont(new Font("Verdana", Font.BOLD, 15));
		orderArea.setForeground(Color.RED);
		add(orderArea);
	
	}

	@Override
	public void update(Object o, Object p) {
		Order selectedOrder = (Order)o;
		@SuppressWarnings("unchecked")
		ArrayList<MenuItem> selectedMenu = (ArrayList<MenuItem>)p;
		ArrayList<String> selectedMenuItemNames = new ArrayList<String>();
		for(MenuItem mi: selectedMenu)
			selectedMenuItemNames.add(mi.getMenuItemName());
		orderArea.append("Please prepare order " + selectedOrder.getOrderID() + " consisting of " + selectedMenuItemNames +  "\n");
		
	}
}
