package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import model.MenuItem;

import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;

public class MainViewFrame extends RestaurantFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);

	private JPanel administratorTab = new JPanel();
	private JPanel addCompositeProduct = new JPanel();
	private JPanel editProduct = new JPanel();
	private JTextField baseProductName;
	private JTextField baseProductPrice;
	private JButton btnAddBaseProduct = new JButton("Add base product");
	private JTable table = null;
	private JScrollPane scrollPane = new JScrollPane();
	private JList<MenuItem> list;
	private JScrollPane compositeProductScrollPane = new JScrollPane();
	private JButton btnAddCompositeProduct = new JButton("Add composite product");
	private JTextField compositeProductName;

	private ArrayList<MenuItem> selectedList = null;
	private JTextField deletedMenuItemValue = new JTextField();
	private JButton btnDelete = new JButton("Delete product");

	private JScrollPane scrollPane_2 = new JScrollPane();
	private JList<MenuItem> allComponents = new JList<MenuItem>();
	private JButton btnEditProduct = new JButton("Edit product");

	private JTextField newProductName = new JTextField();
	private JTextField editedProductName;
	private JTextField newProductPrice;
	private JTextField tableNameField;

	private JPanel waiterTab = new JPanel();

	private JScrollPane finishedOrdersScrollPane = new JScrollPane();
	private JScrollPane menuScrollPane = new JScrollPane();
	private JTable menuTable;
	private JTable ordersTable;
	private JButton createOrder = new JButton("Create order");
	private JButton btnGenerateBills = new JButton("Generate bills");

	private ChefPane chefGUI;

	public MainViewFrame(String title) {
		super(title);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		tabbedPane.setBounds(10, 11, 874, 649);
		contentPane.add(tabbedPane);

		tabbedPane.addTab("Administrator", null, administratorTab, null);
		administratorTab.setLayout(null);

		tabbedPane_1.setBounds(419, 11, 440, 599);
		administratorTab.add(tabbedPane_1);

		JPanel addBaseProduct = new JPanel();
		tabbedPane_1.addTab("Add base product", null, addBaseProduct, null);
		addBaseProduct.setLayout(null);

		baseProductName = new JTextField();
		baseProductName.setBounds(109, 200, 301, 42);
		addBaseProduct.add(baseProductName);
		baseProductName.setColumns(10);

		baseProductPrice = new JTextField();
		baseProductPrice.setColumns(10);
		baseProductPrice.setBounds(109, 253, 301, 42);
		addBaseProduct.add(baseProductPrice);

		btnAddBaseProduct.setBounds(109, 306, 301, 23);
		addBaseProduct.add(btnAddBaseProduct);

		JLabel lblProductPrice = new JLabel("Price:");
		lblProductPrice.setBounds(31, 267, 46, 14);
		addBaseProduct.add(lblProductPrice);

		JLabel lblProductName = new JLabel("Name:");
		lblProductName.setBounds(31, 214, 46, 14);
		addBaseProduct.add(lblProductName);

		tabbedPane_1.addTab("Add composite product", null, addCompositeProduct, null);
		addCompositeProduct.setLayout(null);

		JLabel toolTipCompositeProduct = new JLabel("Choose more products");
		toolTipCompositeProduct.setHorizontalAlignment(SwingConstants.CENTER);
		toolTipCompositeProduct.setBounds(10, 4, 138, 34);
		addCompositeProduct.add(toolTipCompositeProduct);

		btnAddCompositeProduct.setBounds(119, 513, 223, 34);
		addCompositeProduct.add(btnAddCompositeProduct);

		JLabel toolTipCompositeProduct2 = new JLabel("New product name:");
		toolTipCompositeProduct2.setHorizontalAlignment(SwingConstants.CENTER);
		toolTipCompositeProduct2.setBounds(10, 33, 118, 34);
		addCompositeProduct.add(toolTipCompositeProduct2);

		compositeProductName = new JTextField();
		compositeProductName.setBounds(138, 40, 287, 20);
		addCompositeProduct.add(compositeProductName);
		compositeProductName.setColumns(10);

		compositeProductScrollPane.setBounds(10, 67, 415, 435);
		addCompositeProduct.add(compositeProductScrollPane);

		JPanel deleteProduct = new JPanel();
		tabbedPane_1.addTab("Delete product", null, deleteProduct, null);
		deleteProduct.setLayout(null);

		deletedMenuItemValue.setBounds(20, 132, 405, 34);
		deleteProduct.add(deletedMenuItemValue);
		deletedMenuItemValue.setColumns(10);

		JLabel lblDelete = new JLabel("Item to be deleted:");
		lblDelete.setBounds(20, 107, 249, 14);
		deleteProduct.add(lblDelete);

		JLabel lblDelete1 = new JLabel("Select an item from the table");
		lblDelete1.setBounds(20, 32, 249, 14);
		deleteProduct.add(lblDelete1);

		btnDelete.setBounds(120, 177, 219, 42);
		deleteProduct.add(btnDelete);

		tabbedPane_1.addTab("Edit product", null, editProduct, null);
		editProduct.setLayout(null);

		JLabel lblToBeEdited = new JLabel("Selected item");
		lblToBeEdited.setBounds(72, 90, 82, 14);
		editProduct.add(lblToBeEdited);

		JLabel lblSelectNewComponents = new JLabel("Select new components:");
		lblSelectNewComponents.setBounds(244, 11, 156, 14);
		editProduct.add(lblSelectNewComponents);

		scrollPane_2.setBounds(227, 33, 198, 373);
		editProduct.add(scrollPane_2);

		scrollPane_2.setViewportView(allComponents);

		newProductName.setBounds(147, 417, 278, 20);
		editProduct.add(newProductName);
		newProductName.setColumns(10);

		JLabel lblNewName = new JLabel("Enter new name:");
		lblNewName.setBounds(20, 417, 117, 20);
		editProduct.add(lblNewName);

		btnEditProduct.setBounds(100, 505, 259, 43);
		editProduct.add(btnEditProduct);

		editedProductName = new JTextField();
		editedProductName.setEditable(false);
		editedProductName.setBounds(10, 107, 207, 35);
		editProduct.add(editedProductName);

		editedProductName.setColumns(10);

		newProductPrice = new JTextField();
		newProductPrice.setColumns(10);
		newProductPrice.setBounds(147, 452, 278, 20);
		editProduct.add(newProductPrice);

		JLabel lblNewPrice = new JLabel("Enter new price:");
		lblNewPrice.setBounds(20, 452, 117, 20);
		editProduct.add(lblNewPrice);
		
		JTextPane txtpnForABase = new JTextPane();
		txtpnForABase.setEnabled(false);
		txtpnForABase.setEditable(false);
		txtpnForABase.setText("For a base product, enter the new name and the new price\r\n\r\nFor a composite product, select only the list of new products and enter the new name (price will be calculated automatically)");
		txtpnForABase.setBounds(37, 180, 149, 175);
		editProduct.add(txtpnForABase);

		tabbedPane.addTab("Waiter", null, waiterTab, null);
		waiterTab.setLayout(null);

		waiterTab.add(menuScrollPane);

		menuTable = new JTable();
		menuScrollPane.setViewportView(menuTable);

		waiterTab.add(finishedOrdersScrollPane);

		ordersTable = new JTable();
		finishedOrdersScrollPane.setViewportView(ordersTable);

		JLabel lblMadeOrders = new JLabel("Finished orders:");
		lblMadeOrders.setBounds(428, 313, 154, 14);
		waiterTab.add(lblMadeOrders);

		JLabel lblNewLabel = new JLabel("Select items from the menu on the left");
		lblNewLabel.setBounds(428, 41, 292, 14);
		waiterTab.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Enter the table which made the order:");
		lblNewLabel_1.setBounds(428, 66, 230, 14);
		waiterTab.add(lblNewLabel_1);

		tableNameField = new JTextField();
		tableNameField.setBounds(496, 91, 327, 20);
		waiterTab.add(tableNameField);
		tableNameField.setColumns(10);

		createOrder.setBounds(587, 116, 130, 44);
		waiterTab.add(createOrder);

		btnGenerateBills.setBounds(587, 487, 121, 44);
		waiterTab.add(btnGenerateBills);
	}

	public void setCloseAction(WindowListener al) {
		this.addWindowListener(al);
	}

	public void setAddBaseProductActionListener(ActionListener al) {
		this.btnAddBaseProduct.addActionListener(al);
	}

	public void setAddCompositeProductActionListener(ActionListener al) {
		this.btnAddCompositeProduct.addActionListener(al);
	}

	public JTable getTable() {
		return this.table;
	}

	public void setTable(JTable table) {
		if (table != null) {
			if (this.table != null) {
				scrollPane.remove(this.table);
			}
			this.table = table;
			scrollPane.setViewportView(this.table);
			scrollPane.setBounds(10, 11, 404, 599);
			administratorTab.add(scrollPane);
			scrollPane.revalidate();
			scrollPane.repaint();
		}
	}

	public void setMenuTable(JTable table) {
		if (table != null) {
			if (this.menuTable != null) {
				scrollPane.remove(this.menuTable);
			}
			this.menuTable = table;
			menuScrollPane.setViewportView(this.menuTable);
			menuScrollPane.setBounds(10, 11, 408, 599);
			waiterTab.add(menuScrollPane);
			menuScrollPane.revalidate();
			menuScrollPane.repaint();
		}
	}

	public void setOrderTable(JTable table) {
		if (table != null) {
			if (this.ordersTable != null) {
				finishedOrdersScrollPane.remove(this.ordersTable);
			}
			this.ordersTable = table;
			finishedOrdersScrollPane.setViewportView(this.ordersTable);
			finishedOrdersScrollPane.setBounds(428, 327, 431, 149);
			waiterTab.add(finishedOrdersScrollPane);
			finishedOrdersScrollPane.revalidate();
			finishedOrdersScrollPane.repaint();
		}
	}

	public String getBaseProductName() {
		return this.baseProductName.getText().trim();
	}

	public Double getBaseProductPrice() {
		if (this.baseProductPrice.getText().trim().isEmpty())
			return 0.0;
		else
			return Double.parseDouble(this.baseProductPrice.getText().trim());
	}

	public String getCompositeProductName() {
		return this.compositeProductName.getText().trim();
	}

	public void setBaseProducts(final JList<MenuItem> list1) {
		if (list1 != null) {
			if (this.list != null) {
				compositeProductScrollPane.remove(list1);
			}
			this.list = list1;
			list.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e) {
				}

				@Override
				public void mousePressed(MouseEvent e) {
				}

				@Override
				public void mouseReleased(MouseEvent e) {

					selectedList = (ArrayList<MenuItem>) list1.getSelectedValuesList();
				}

				@Override
				public void mouseEntered(MouseEvent e) {
				}

				@Override
				public void mouseExited(MouseEvent e) {
				}
			});
			compositeProductScrollPane.setBounds(10, 78, 415, 420);
			compositeProductScrollPane.setViewportView(list1);
			this.revalidate();
		}
	}

	public void setAllBaseProducts(final JList<MenuItem> list1) {
		if (list1 != null) {
			if (this.allComponents != null) {
				scrollPane_2.remove(list1);
			}
			this.allComponents = list1;
			allComponents.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e) {
				}

				@Override
				public void mousePressed(MouseEvent e) {
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					selectedList = (ArrayList<MenuItem>) list1.getSelectedValuesList();
				}

				@Override
				public void mouseEntered(MouseEvent e) {
				}

				@Override
				public void mouseExited(MouseEvent e) {
				}
			});
			scrollPane_2.setBounds(227, 33, 198, 373);
			scrollPane_2.setViewportView(list1);
			;
			this.revalidate();
		}
	}

	public JPanel getAdminPane() {
		return this.administratorTab;
	}

	public JScrollPane getItemsScrollPane() {
		return this.scrollPane_2;
	}

	public void setEditedProductName(String text) {
		this.editedProductName.setText(text);
	}

	public String getEditedProductName() {
		return editedProductName.getText();
	}

	public String getNewProductName() {
		return this.newProductName.getText();
	}

	public String getNewProductPrice() {
		return this.newProductPrice.getText();
	}

	public ArrayList<MenuItem> getList() {
		return this.selectedList;
	}

	public ArrayList<String> getOrderedList() {
		int[] indices = this.menuTable.getSelectedRows();
		ArrayList<String> orderedItems = new ArrayList<String>();
		for (int i : indices)
			orderedItems.add(this.menuTable.getModel().getValueAt(i, 0).toString());
		return orderedItems;
	}

	public String getTableName() {
		return this.tableNameField.getText();
	}

	public void setDeleteProductField(String text) {
		this.deletedMenuItemValue.setText(text);
	}

	public String getDeleteProductField() {
		return this.deletedMenuItemValue.getText().toString();
	}

	public void setDeleteButtonActionListener(ActionListener al) {
		this.btnDelete.addActionListener(al);
	}

	public void setEditButtonActionListener(ActionListener al) {
		this.btnEditProduct.addActionListener(al);
	}

	public void setAddOrderButtonActionListener(ActionListener al) {
		this.createOrder.addActionListener(al);
	}

	public void setGenerateButtonActionListener(ActionListener al) {
		this.btnGenerateBills.addActionListener(al);
	}

	public void setChefPane(ChefPane cp) {
		this.chefGUI = cp;
		tabbedPane.addTab("Chef", null, chefGUI, null);
	}

	public ChefPane getChefPane() {
		return this.chefGUI;
	}
	public void clearAllInputs() {
		this.baseProductName.setText("");
		this.baseProductPrice.setText("");
		this.compositeProductName.setText("");
		this.deletedMenuItemValue.setText("");
		this.newProductName.setText("");
		this.newProductPrice.setText("");
		this.selectedList = null;
	}
}
