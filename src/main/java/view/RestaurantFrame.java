package view;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public abstract class RestaurantFrame extends JFrame{

	private static final long serialVersionUID = 1L;

	protected static JPanel contentPanel;
	
	public RestaurantFrame(String title)
	{
		setTitle(title);
		setSize(900,700);
		setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		UIManager.put("TabbedPane.selected", new Color(10, 22, 18));		
		UIManager.put("TabbedPane.foreground",new Color(247, 207, 62));
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5,5,5,5));
		add(contentPanel, BorderLayout.CENTER);
	}
	
}
