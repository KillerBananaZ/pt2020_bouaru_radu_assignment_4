package observer;

import java.util.ArrayList;

import model.MenuItem;
import model.Order;

public class Observable {
	
	private static ArrayList<Observer> observers = new ArrayList<>();
	
	private ArrayList<MenuItem> menu;
	private Order o;
	
	public void addObserver(Observer observer)
	{
		observers.add(observer);
	}
	public int getObserverCount() {
		return observers.size();
	}
	public void notifyObservers(Order o, ArrayList<MenuItem> orderedMenu)
	{
		this.o = o;
		this.menu = orderedMenu;
		for(Observer observer : observers)
			observer.update(this.o, this.menu);
	}
}
